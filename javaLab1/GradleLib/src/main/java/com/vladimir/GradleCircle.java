package com.vladimir;

public class GradleCircle {
    protected int radius;
    protected int x;
    protected int y;

    public GradleCircle(int r, int x, int y) {
        radius = r;
        this.x = x;
        this.y = y;
    }

    public double getArea() {
        return Math.PI  * Math.pow(radius, 2);
    }
}
