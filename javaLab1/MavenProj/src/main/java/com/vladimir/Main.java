package com.vladimir;

public class Main {
    public static void main(String[] args) {
        Circle c = new Circle(10, 3, 2);

        System.out.println("Maven library:");
        System.out.println("Square:" + c.getArea());
        System.out.println("-----------------------------");

        GradleCircle g = new GradleCircle(17, 5, 13);

        System.out.println("Gradle library:");
        System.out.println("Square:" + g.getArea());
    }
}
