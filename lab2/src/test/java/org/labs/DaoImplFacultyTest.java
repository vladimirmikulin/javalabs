package org.labs;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.labs.daoImplementation.DaoImpl;
import org.labs.models.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@DisplayName("Faculty")
@ExtendWith(MockitoExtension.class)
public class DaoImplFacultyTest extends DaoImplTest{
    @Test
    @DisplayName("getEntity_ByID")
    void testGetFacultyById() throws SQLException {
        DaoImpl<Faculty> facultyDao = new DaoImpl<>(Faculty.class, mockConnection);
        Faculty facultyId12 = new Faculty(12,"FPM", 20);
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getObject(anyString())).thenAnswer(invocationOnMock -> {
            switch ((String) invocationOnMock.getArguments()[0]) {
                case "faculty_name":            return facultyId12.getFaculty_name();
                case "id":              return facultyId12.getId();
                case "groups_number":   return facultyId12.getGroups_number();
            }
            return null;
        });
        assertEquals(facultyId12, facultyDao.getEntity_byID(12));
        assertNull(facultyDao.getEntity_byID(4));
    }

    @Test
    @DisplayName("getEntity_List")
    void testGetFacultyList() throws SQLException {
        long[] ids = new long[] { 0L, 678567L };
        String[] faculty_names = new String[] { "IPSA", "FIOT" };
        int[] group_numbers = new int[] { 31, 27 };
        DaoImpl<Faculty> facultyDao = new DaoImpl<>(Faculty.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(facultyDao.getEntity_List().size(), 0);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countFacultyNames, countGroupsNumbers = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "faculty_name":        return faculty_names[countIds++];
                    case "id":                  return ids[countFacultyNames++];
                    case "groups_number":       return group_numbers[countGroupsNumbers++];
                }
                return null;
            }
        });
        var list = facultyDao.getEntity_List();
        var expectedList = new ArrayList<>() {{
            add(new Faculty(ids[0], faculty_names[0], group_numbers[0]));
            add(new Faculty(ids[1], faculty_names[1], group_numbers[1]));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
