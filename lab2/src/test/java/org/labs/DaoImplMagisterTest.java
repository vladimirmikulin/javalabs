package org.labs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.labs.daoImplementation.DaoImpl;
import org.labs.models.Bachelor;
import org.labs.models.Magister;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Magister")
@ExtendWith(MockitoExtension.class)
public class DaoImplMagisterTest extends DaoImplTest{
    @Test
    @DisplayName("getEntity_ById")
    void testGetMagisterById() throws SQLException {
        DaoImpl<Magister> magisterDao = new DaoImpl<>(Magister.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        Magister magisterId38 = new Magister(38,"Vladimir Mikulin",19,9,14, "Pravo Kievan Rus",191.2);
        when(mockResultSet.getObject(anyString())).thenAnswer(invocationOnMock -> {
            switch ((String) invocationOnMock.getArguments()[0]) {
                case "id":                      return magisterId38.getId();
                case "full_name":               return magisterId38.getFull_name();
                case "age":                     return magisterId38.getAge();
                case "groups_id":               return magisterId38.getGroups_id();
                case "faculties_id":            return magisterId38.getFaculties_id();
                case "diploma_descr":           return magisterId38.getDiploma_descr();
                case "average_diploma":         return magisterId38.getAverage_diploma();
            }
            return null;
        });
        assertEquals(magisterId38, magisterDao.getEntity_byID(38));
        assertNull(magisterDao.getEntity_byID(58));
    }

    @Test
    @DisplayName("getEntity_List")
    void testMagisterList() throws SQLException {
        DaoImpl<Magister> magisterDao = new DaoImpl<>(Magister.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(magisterDao.getEntity_List().size(), 0);
        Magister oneMagister = new Magister(29,"Kostia Lelikov", 22, 7, 16, "Web-service for KPI schedule", 199.4);
        Magister anotherMagister = new Magister(6538,"Lina Dovganyuk", 23, 382, 20,"Java app for computer class", 186.3);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countFullName, countAge, countGroupsId, countFacultiesID, countDiplomaDescr,countAverageDiploma;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":                  return countIds++ == 0 ? oneMagister.getId() : anotherMagister.getId();
                    case "full_name":           return countFullName++ == 0 ? oneMagister.getFull_name() : anotherMagister.getFull_name();
                    case "age":                 return countAge++ == 0 ? oneMagister.getAge() : anotherMagister.getAge();
                    case "groups_id":           return countGroupsId++ == 0 ? oneMagister.getGroups_id() : anotherMagister.getGroups_id();
                    case "faculties_id":        return countFacultiesID++ == 0 ? oneMagister.getFaculties_id() : anotherMagister.getFaculties_id();
                    case "diploma_descr":           return countDiplomaDescr++ == 0 ? oneMagister.getDiploma_descr() : anotherMagister.getDiploma_descr();
                    case "average_diploma":         return countAverageDiploma++ == 0 ? oneMagister.getAverage_diploma() : anotherMagister.getAverage_diploma();
                }
                return null;
            }
        });
        var list = magisterDao.getEntity_List();
        var expectedList = new ArrayList<>() {{ add(oneMagister); add(anotherMagister); }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
