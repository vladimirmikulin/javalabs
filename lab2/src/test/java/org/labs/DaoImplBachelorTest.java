package org.labs;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.labs.daoImplementation.DaoImpl;
import org.labs.models.Bachelor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Bachelor")
@ExtendWith(MockitoExtension.class)
public class DaoImplBachelorTest extends DaoImplTest {
    @Test
    @DisplayName("getEntity_ById")
    void testGetBachelorById() throws SQLException {
        DaoImpl<Bachelor> bachelorDao = new DaoImpl<>(Bachelor.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        Bachelor bachelorId38 = new Bachelor(38,"Vladimir Mikulin",19,9,14, "Pravo Kievan Rus",190.22);
        when(mockResultSet.getObject(anyString())).thenAnswer(invocationOnMock -> {
            switch ((String) invocationOnMock.getArguments()[0]) {
                case "id":                return bachelorId38.getId();
                case "full_name":         return bachelorId38.getFull_name();
                case "age":               return bachelorId38.getAge();
                case "groups_id":         return bachelorId38.getGroups_id();
                case "faculties_id":      return bachelorId38.getFaculties_id();
                case "zno_descr":         return bachelorId38.getZno_descr();
                case "average_zno":       return bachelorId38.getAverage_zno();
            }
            return null;
        });
        assertEquals(bachelorId38, bachelorDao.getEntity_byID(38));
        assertNull(bachelorDao.getEntity_byID(58));
    }// как всегда да

    @Test
    @DisplayName("getEntity_List")
    void testBachelorList() throws SQLException {
        DaoImpl<Bachelor> bachelorDao = new DaoImpl<>(Bachelor.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(bachelorDao.getEntity_List().size(), 0);
        Bachelor oneBachelor = new Bachelor(29,"Arkadiy Kravchuk", 20, 7, 16, "Kozak v pole voin", 199.4);
        Bachelor anotherBachelor = new Bachelor(6538,"Oleksiy Atamanyuik", 19, 382, 20,"Math in Heart", 132.3);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countWeights, countInsuredValues, countInvoiceIds,
                    countRadius, countForLorries, count = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":                  return countIds++ == 0 ? oneBachelor.getId() : anotherBachelor.getId();
                    case "full_name":           return countWeights++ == 0 ? oneBachelor.getFull_name() : anotherBachelor.getFull_name();
                    case "age":                 return countInsuredValues++ == 0 ? oneBachelor.getAge() : anotherBachelor.getAge();
                    case "groups_id":           return countInvoiceIds++ == 0 ? oneBachelor.getGroups_id() : anotherBachelor.getGroups_id();
                    case "faculties_id":        return countRadius++ == 0 ? oneBachelor.getFaculties_id() : anotherBachelor.getFaculties_id();
                    case "zno_descr":           return countForLorries++ == 0 ? oneBachelor.getZno_descr() : anotherBachelor.getZno_descr();
                    case "average_zno":         return count++ == 0 ? oneBachelor.getAverage_zno() : anotherBachelor.getAverage_zno();
                }
                return null;
            }
        });
        var list = bachelorDao.getEntity_List();
        var expectedList = new ArrayList<>() {{ add(oneBachelor); add(anotherBachelor); }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
