package org.labs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.labs.daoImplementation.DaoImpl;
import org.labs.models.Group;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Group")
@ExtendWith(MockitoExtension.class)
public class DaoImplGroupTest  extends DaoImplTest{
    @Test
    @DisplayName("getEntity_ByID")
    void testGetGroupById() throws SQLException {
        DaoImpl<Group> groupDao = new DaoImpl<>(Group.class, mockConnection);
        Group groupId0 = new Group(0,"KP-71", 18);
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getObject(anyString())).thenAnswer(invocationOnMock -> {
            switch ((String) invocationOnMock.getArguments()[0]) {
                case "id":          return groupId0.getId();
                case "groups_name":     return groupId0.getGroups_name();
                case "students_number": return groupId0.getStudents_number();
            }
            return null;
        });
        assertEquals(groupId0, groupDao.getEntity_byID(0));
        assertNull(groupDao.getEntity_byID(15));
    }

    @Test
    @DisplayName("getEntity_List")
    void testGroupList() throws SQLException {
        DaoImpl<Group> groupDao = new DaoImpl<>(Group.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false,true, true, true, false);
        assertEquals(groupDao.getEntity_List().size(), 0);
        long[] ids = new long[] { 4, 23, 45};
        String[] group_names = new String[] { "KP-72", "KV-61", "KM-91" };
        int[] student_numbers = new int[] { 22, 33, 44};
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countGroupsName, countStudentNumbers = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":              return ids[countIds++];
                    case "groups_name":     return group_names[countGroupsName++];
                    case "students_number": return student_numbers[countStudentNumbers++];
                }
                return null;
            }
        });
        var list = groupDao.getEntity_List();
        var expectedList = new ArrayList<>() {{
            add(new Group(ids[0], group_names[0], student_numbers[0]));
            add(new Group(ids[1], group_names[1], student_numbers[1]));
            add(new Group(ids[2], group_names[2], student_numbers[2]));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
