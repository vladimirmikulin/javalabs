package org.labs;

import org.labs.daoImplementation.Dao;
import org.labs.daoImplementation.DaoImpl;
import org.labs.models.*;

public class Main {
    public static void main(String[] args) throws Exception {

        System.out.println("Lab 2 - Java =)");
        System.out.println("================================================================");
        System.out.println("================================================================");
        System.out.println("================================================================");

        Dao dao = new Dao();


        System.out.println("Faculty");
        System.out.println("---------");
        System.out.println(dao.getFaculty(1));
        System.out.println(dao.getFacultyList());
         // -----------------------------------------
        System.out.println("University");
        System.out.println("------------");
        System.out.println(dao.getUniversity(3));
        System.out.println(dao.getUniversityList());
        // ------------------------------------------
        System.out.println("Group");
        System.out.println("---------");
        System.out.println(dao.getGroup(5));
        System.out.println(dao.getGroupList());
        // -----------------------------------------
        System.out.println("Bachelor");
        System.out.println("---------");
        System.out.println(dao.getBachelor(1));
        System.out.println(dao.getBachelorList());
        // -----------------------------------------
        System.out.println("Magister");
        System.out.println("---------");
        System.out.println(dao.getMagister(2));
        System.out.println(dao.getMagisterList());
    }

}