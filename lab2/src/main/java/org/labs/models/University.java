package org.labs.models;

import org.labs.annotation.Entity;

import java.util.Date;
import java.util.Objects;

@Entity(table = "universities", primaryKey = "id")
public class University {

    private long id;
    private String university_name;
    private String rector_name;
    private int faculties_number;
    private Date date_created;


    public University() {}

    public University(long id, String university_name, String rector_name, int faculties_number,Date date_created) {
        this.id = id;
        this.university_name = university_name;
        this.rector_name = rector_name;
        this.faculties_number = faculties_number;
        this.date_created = date_created;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUniversity_name() {
        return university_name;
    }

    public void setUniversity_name(String university_name) {
        this.university_name = university_name;
    }
    public String getRector_name() {
        return rector_name;
    }

    public void setRector_name(String rector_name) {
        this.rector_name = rector_name;
    }

    public int getFaculties_number() {
        return faculties_number;
    }

    public void setFaculties_number(int faculties_number) {
        this.faculties_number = faculties_number;
    }

    public Date getDate_created() {
        return date_created;
    }

    public void setDate_created(Date date_created) {
        this.date_created = date_created;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        University that = (University) o;
        return id == that.id &&
                university_name.equals(that.university_name)   &&
                   rector_name.equals(that.rector_name)        &&
                     faculties_number == that.faculties_number &&
                        date_created.equals(that.date_created);
    }

    @Override
    public String toString() {
        return "University{" +
                "id=" + id +
                ", university_name='" + university_name + '\'' +
                ", rector_name='" + rector_name + '\'' +
                ", faculties_number=" + faculties_number +
                ", date_created=" + date_created +
                '}';
    }


}
