package org.labs.models;

import org.labs.annotation.Entity;

import java.math.BigDecimal;

@Entity(table = "magisters")
public class Magister extends Student{
    private String diploma_descr;
    private double average_diploma;

    public Magister() { }

    public Magister(long id, String full_name, int age, long groups_id, long faculties_id,
                    String diploma_descr, double average_diploma) {
        super(id, full_name, age, groups_id, faculties_id);
        this.diploma_descr = diploma_descr;
        this.average_diploma = average_diploma;
    }

    public String getDiploma_descr() {
        return diploma_descr;
    }

    public void setDiploma_descr(String diploma_descr) {
        this.diploma_descr = diploma_descr;
    }

    public double getAverage_diploma() {
        return average_diploma;
    }

    public void setAverage_diploma(double average_diploma) {
        this.average_diploma = average_diploma;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Magister magister = (Magister) o;
        return diploma_descr.equals(magister.diploma_descr) &&
                average_diploma == magister.average_diploma;
    }

    @Override
    public String toString() {
        return "Magister{" +
                "diploma_descr='" + diploma_descr + '\'' +
                ", average_diploma=" + average_diploma +
                '}';
    }

}
