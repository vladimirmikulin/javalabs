package org.labs.models;

import org.labs.annotation.Entity;

import java.util.Objects;

@Entity(table = "students", primaryKey = "id")
public abstract class Student {
    private long id;
    private String full_name;
    private int age;
    private long groups_id;
    private long faculties_id;

    public Student() { }

    public Student(long id, String full_name, int age, long groups_id, long faculties_id) {
        this.id = id;
        this.full_name = full_name;
        this.age = age;
        this.groups_id = groups_id;
        this.faculties_id = faculties_id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getGroups_id() {
        return groups_id;
    }

    public void setGroups_id(long groups_id) {
        this.groups_id = groups_id;
    }

    public long getFaculties_id() {
        return faculties_id;
    }

    public void setFaculties_id(long faculties_id) {
        this.faculties_id = faculties_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student students = (Student) o;
        return id == students.id &&
                full_name.equals(students.full_name) &&
                age == students.age &&
                groups_id == students.groups_id &&
                faculties_id == students.faculties_id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", full_name='" + full_name + '\'' +
                ", age=" + age +
                ", groups_id=" + groups_id +
                ", faculties_id=" + faculties_id +
                '}';
    }


}
