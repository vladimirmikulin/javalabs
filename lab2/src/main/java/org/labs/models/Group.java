package org.labs.models;

import org.labs.annotation.Entity;

@Entity(table = "groups", primaryKey = "id")
public class Group {


    private long id;
    private String groups_name;
    private int students_number;

    public Group() { }

    public Group(long id, String groups_name, int students_number) {
        this.id = id;
        this.groups_name = groups_name;
        this.students_number = students_number;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroups_name() {
        return groups_name;
    }

    public void setGroups_name(String groups_name) {
        this.groups_name = groups_name;
    }

    public int getStudents_number() {
        return students_number;
    }

    public void setStudents_number(int students_number) {
        this.students_number = students_number;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return id == group.id &&
                groups_name.equals(group.groups_name) &&
                students_number == group.students_number ;
    }
    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", groups_name='" + groups_name + '\'' +
                ", students_number=" + students_number +
                '}';
    }


}
