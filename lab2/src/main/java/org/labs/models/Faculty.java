package org.labs.models;

import org.labs.annotation.Entity;

import java.util.Objects;

@Entity(table = "faculties",  primaryKey = "id")
public class Faculty {

    private long id;
    private String faculty_name;
    private int groups_number;

    public Faculty() { }

    public Faculty(long id, String faculty_name, int groups_number) {
        this.id = id;
        this.faculty_name = faculty_name;
        this.groups_number = groups_number;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFaculty_name() {
        return faculty_name;
    }

    public void setFaculty_name(String faculty_name) {
        this.faculty_name = faculty_name;
    }

    public int getGroups_number() {
        return groups_number;
    }

    public void setGroups_number(int groups_number) {
        this.groups_number = groups_number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Faculty faculty = (Faculty) o;
        return id == faculty.id &&
                faculty_name.equals(faculty.faculty_name) &&
                groups_number == faculty.groups_number;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "id=" + id +
                ", faculty_name='" + faculty_name + '\'' +
                ", groups_number=" + groups_number +
                '}';
    }


}
