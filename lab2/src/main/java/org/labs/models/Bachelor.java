package org.labs.models;

import org.labs.annotation.Entity;

import java.math.BigDecimal;

@Entity(table = "bachelors")
public class Bachelor extends Student {
    private String zno_descr;
    private double average_zno;

    public Bachelor() { }

    public Bachelor(long id, String full_name, int age, long groups_id, long faculties_id,
                    String zno_descr, double average_zno) {
        super(id, full_name, age, groups_id, faculties_id);
        this.zno_descr = zno_descr;
        this.average_zno = average_zno;
    }
    public String getZno_descr() {
    return zno_descr;
}

    public void setZno_descr(String zno_descr) {
        this.zno_descr = zno_descr;
    }

    public double getAverage_zno() {
        return average_zno;
    }

    public void setAverage_zno(double average_zno) {
        this.average_zno = average_zno;
    }

    @Override
    public boolean equals(Object o) { //СРАВНЕНИЕ ОБЬЕКТОВ ДЛЯ ТЕСТОВ
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Bachelor bachelor = (Bachelor) o;
        return zno_descr.equals(bachelor.zno_descr) &&
                average_zno == bachelor.average_zno;
    }

    @Override
    public String toString() {
        return "Bachelor{" +
                "zno_descr='" + zno_descr + '\'' +
                ", average_zno=" + average_zno +
                '}';
    }

}
