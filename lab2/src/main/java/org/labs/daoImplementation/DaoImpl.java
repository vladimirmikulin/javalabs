package org.labs.daoImplementation;

import org.labs.annotation.Entity;
import org.postgresql.ds.PGSimpleDataSource;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class DaoImpl<T> implements IDaoImpl<T> {
    private Class<T> clazz;
    private Connection connection;

    public DaoImpl(Class<T> clazz) throws Exception {
        this.clazz = clazz;
        Properties properties = new Properties();
        var classloader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classloader.getResourceAsStream("database/mydatabase.properties");
        if (inputStream == null) {
            throw new Exception("Not read database settings!!!!");
        }
        properties.load(inputStream);
        var pgDataSource = new PGSimpleDataSource();
        pgDataSource.setServerName(properties.getProperty("DB_HOST"));
        pgDataSource.setDatabaseName(properties.getProperty("DB_NAME"));
        pgDataSource.setPortNumber(Integer.parseInt(properties.getProperty("DB_PORT")));
        pgDataSource.setUser(properties.getProperty("DB_USERNAME"));
        pgDataSource.setPassword(properties.getProperty("DB_PASSWORD"));
        connection = pgDataSource.getConnection();
    }

    public DaoImpl(Class<T> clazz, Connection connection) {
        this.clazz = clazz;
        this.connection = connection;
    }

    private static String getTableName(Class<?> clazz) {
        return clazz.getAnnotation(Entity.class).table();
    }

    private static boolean isInheritedEntity(Class<?> clazz) {
        return clazz.getSuperclass().isAnnotationPresent(Entity.class);
    }

    private static ArrayList<Field> getAllColumns(Class<?> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        return new ArrayList<>(Arrays.asList(fields));
    }
    private static String buildSelectQuery(Class<?> clazz, Long id) {
        StringBuilder query = new StringBuilder("SELECT * FROM ");
        String tableName = getTableName(clazz);
        //query.append(buildSelectQueryForColumns(clazz, id != null));
        query.append(tableName).append(' ');
        ArrayList<String> inheritedTableNames = new ArrayList<>();
        while (isInheritedEntity(clazz)) {
            clazz = clazz.getSuperclass();
            inheritedTableNames.add(getTableName(clazz));
        }
        String primaryKey = clazz.getAnnotation(Entity.class).primaryKey();
        if (!inheritedTableNames.isEmpty()) {
            inheritedTableNames.add(0, tableName);
            query.append("INNER JOIN ");
            for (int i = 1; i < inheritedTableNames.size(); i++) {
                query.append(inheritedTableNames.get(i));
                query.append(" on ").append(inheritedTableNames.get(i - 1));
                query.append('.').append(primaryKey);
                query.append(" = ").append(inheritedTableNames.get(i));
                query.append('.').append(primaryKey).append(' ');
            }
        }
        if (id != null) {
            query.append("WHERE ").append(tableName);
            query.append('.').append(primaryKey);
            query.append(" = ").append(id);
        }
        return query.toString();
    }


    private T createEntity(ResultSet resultSet) throws Exception {
        T item = clazz.getDeclaredConstructor().newInstance();
        List<Field> columns = new ArrayList<>();
        Class<?> currentClazz = null;
        do {
            currentClazz = currentClazz == null ? clazz : currentClazz.getSuperclass();
            columns.addAll(getAllColumns(currentClazz));
        } while (isInheritedEntity(currentClazz));
        for (var column : columns) {
            column.setAccessible(true);
            column.set(item, resultSet.getObject(column.getName()));
        }
        return item;
    }

    public T getEntity_byID(long id) {
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(buildSelectQuery(clazz, id));
            if (!rs.next()) {
                return null;
            }
            return createEntity(rs);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<T> getEntity_List() {
        ArrayList<T> result = new ArrayList<>();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(buildSelectQuery(clazz, null));
            while(rs.next()) {
                result.add(createEntity(rs));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
