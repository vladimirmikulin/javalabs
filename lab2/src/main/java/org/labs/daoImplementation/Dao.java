package org.labs.daoImplementation;

import org.labs.models.*;

import java.util.List;

public class Dao {
    private IDaoImpl<Group> groupIDaoImpl = new DaoImpl<>(Group.class);
    private IDaoImpl<University> universityIDaoImpl = new DaoImpl<>(University.class);
    private IDaoImpl<Magister> magisterIDaoImpl = new DaoImpl<>(Magister.class);
    private IDaoImpl<Bachelor> bachelorIDaoImpl = new DaoImpl<>(Bachelor.class);
    private IDaoImpl<Faculty> facultyIDaoImpl = new DaoImpl<>(Faculty.class);

    public Dao() throws Exception {}

    //------------------------------------------------------- group

    public Group getGroup(long id) {
        return groupIDaoImpl.getEntity_byID(id);
    }
    public List<Group> getGroupList() {
        return groupIDaoImpl.getEntity_List();
    }

    //------------------------------------------------------- university

    public University getUniversity(long id) {
        return universityIDaoImpl.getEntity_byID(id);
    }
    public List<University> getUniversityList() {
        return universityIDaoImpl.getEntity_List();
    }

    //------------------------------------------------------- magister

    public Magister getMagister(long id) {
        return magisterIDaoImpl.getEntity_byID(id);
    }
    public List<Magister> getMagisterList() {
        return magisterIDaoImpl.getEntity_List();
    }
    //------------------------------------------------------- bachelor ну типа

    public Bachelor getBachelor(long id) {
        return bachelorIDaoImpl.getEntity_byID(id);
    }
    public List<Bachelor> getBachelorList() {
        return bachelorIDaoImpl.getEntity_List();
    }

    //------------------------------------------------------- faculty
    public Faculty getFaculty(long id) {
        return facultyIDaoImpl.getEntity_byID(id);
    }
    public List<Faculty> getFacultyList() {
        return facultyIDaoImpl.getEntity_List();
    }
}

