package org.labs.daoImplementation;

import java.util.List;

public interface IDaoImpl<T> {

    T getEntity_byID(long id);
    List<T> getEntity_List();

}
