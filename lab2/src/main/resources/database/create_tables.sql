CREATE TABLE IF NOT EXISTS faculties
(
    id              	     serial PRIMARY KEY,
    faculty_name           text   NOT NULL,
    groups_number          int NOT NULL
);

CREATE TABLE IF NOT EXISTS groups
(
    id                 serial PRIMARY KEY,
    groups_name        text NOT NULL,
    students_number    int NOT NULL
);

CREATE TABLE IF NOT EXISTS universities
(
    id               serial PRIMARY KEY,
    university_name  text NOT NULL,
    rector_name      text NOT NULL,
    faculties_number int NOT NULL,
    date_created     date,
);

CREATE TABLE IF NOT EXISTS students
(
    id                     serial PRIMARY KEY,
    full_name              text NOT NULL,
    age                    int NOT NULL,
    groups_id         	   serial NOT NULL,
    faculties_id      	   serial NOT NULL,
    CONSTRAINT groups_id FOREIGN KEY (groups_id) REFERENCES groups(id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
    CONSTRAINT faculties_id FOREIGN KEY (faculties_id) REFERENCES faculties(id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS bachelors
(
    id                       	            serial PRIMARY KEY,
    zno_descr          			              text NOT NULL,
    average_zno                           decimal NOT NULL,
    CONSTRAINT id FOREIGN KEY (id) REFERENCES students (id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS magisters
(
    id                   serial PRIMARY KEY,
    diploma_descr        text NOT NULL,
    average_diploma      decimal NOT NULL,
    CONSTRAINT id FOREIGN KEY (id)  REFERENCES students (id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
);
