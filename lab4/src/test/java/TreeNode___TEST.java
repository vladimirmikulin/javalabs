import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import Trees.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@DisplayName("___TreeNode___")
public class TreeNode___TEST {
    @Test
    @DisplayName("Single node with null data")
    void SingleTreeNodeOfNullTest() {
        TreeNode<Object> node = new TreeNode<>(null);
        assertNotNull(node);
        assertNull(node.getData());
        assertNull(node.getParent());
        var children = node.getChildren();
        assertNotNull(children);
        assertEquals(0, children.size());
    }

    @Test
    @DisplayName("Single node with arbitrary data")
    void SingleTreeNodeTest() {
        TreeNode<Integer> nodeWithInteger = new TreeNode<>(6);
        assertEquals(Integer.valueOf(6), nodeWithInteger.getData());
        nodeWithInteger.setData(14);
        assertEquals(Integer.valueOf(14), nodeWithInteger.getData());

        TreeNode<String> nodeWithString = new TreeNode<>("TeSt");
        assertEquals("TeSt", nodeWithString.getData());
        nodeWithString.setData("");
        assertEquals("", nodeWithString.getData());

        TreeNode<Double> nodeWithDouble = new TreeNode<>(2.88034);
        assertEquals(Double.valueOf(2.88034), nodeWithDouble.getData());
        nodeWithDouble.setData(0D);
        assertEquals(Double.valueOf(0), nodeWithDouble.getData());
    }

    @Test
    @DisplayName("Node with parent")
    void TreeNodeParentTest() {
        TreeNode<Character> nodeA = new TreeNode<>('A');
        TreeNode<Character> nodeB = new TreeNode<>('B');
        TreeNode<Character> nodeC = new TreeNode<>('C');

        nodeB.setParent(nodeA);
        nodeC.setParent(nodeA);
        assertEquals(nodeA, nodeB.getParent());
        assertEquals(nodeA, nodeC.getParent());

        nodeC.setParent(nodeB);
        assertEquals(nodeB, nodeC.getParent());

        nodeB.setParent(null);
        assertNull(nodeB.getParent());
    }

    @Test
    @DisplayName("Node with children")
    void TreeNodeChildrenTest() {
        TreeNode<Character> nodeA = new TreeNode<>('A');
        TreeNode<Character> nodeB = new TreeNode<>('B');
        TreeNode<Character> nodeC = new TreeNode<>('C');
        TreeNode<Character> nodeD = new TreeNode<>('D');

        ArrayList<TreeNode<Character>> list = new ArrayList<>(Arrays.asList(nodeB, nodeC, nodeD));
        nodeA.setChildren(list);
        assertThat(nodeA.getChildren(), is(list));

        assertEquals(nodeB, nodeA.getChild(0));
        assertEquals(nodeD, nodeA.getChild(2));
        assertThrows(IndexOutOfBoundsException.class, () -> nodeA.getChild(3));
        assertThrows(IndexOutOfBoundsException.class, () -> nodeA.getChild(-1));

        TreeNode<Character> nodeE = new TreeNode<>('E');
        assertThrows(IndexOutOfBoundsException.class, () -> nodeA.setChild(3, nodeE));
        assertThrows(IndexOutOfBoundsException.class, () -> nodeA.setChild(-1, nodeE));
        nodeA.setChild(0, nodeE);
        assertEquals(nodeE, nodeA.getChild(0));
        assertThat(nodeA.getChildren(), is(Arrays.asList(nodeE, nodeC, nodeD)));
    }

    @Test
    @DisplayName("Node linking children with parents")
    void TreeNodeLinkingTest() {
        TreeNode<Character> nodeA = new TreeNode<>('A');
        TreeNode<Character> nodeB = new TreeNode<>('B', nodeA);
        TreeNode<Character> nodeC = new TreeNode<>('C', nodeA);
        TreeNode<Character> nodeD = new TreeNode<>('D', nodeA);

        var children = nodeA.getChildren();
        assertThat(children, is(Arrays.asList(nodeB, nodeC, nodeD)));
        children.forEach(x -> assertEquals(x.getParent(), nodeA));

        nodeA.addChild('E');
        var nodeE = nodeA.getChild(3);
        assertEquals(Character.valueOf('E'), nodeE.getData());
        assertEquals(nodeA, nodeE.getParent());

        nodeA.addChildren(Arrays.asList('F', 'G', 'H'));
        assertEquals(7, nodeA.getChildren().size());
        assertEquals(nodeA, nodeA.getChild(5).getParent());
    }
}
