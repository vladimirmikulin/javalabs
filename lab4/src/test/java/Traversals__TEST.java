import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import Trees.TreeNode;


import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static Trees.Traversals.*;

import java.util.Arrays;
import java.util.Collections;

@DisplayName("___Traversals___")
public class Traversals__TEST {
    private TreeNode<Character> root;

    @BeforeEach
    void createTree() {
        root = new TreeNode<>('F');
        var B = new TreeNode<>('B', root);
        var G = new TreeNode<>('G', root);
        B.addChild('A');
        var D = new TreeNode<>('D', B);
        D.addChildren(Arrays.asList('C', 'E'));
        var I = new TreeNode<>('I', G);
        G.setChildren(Arrays.asList(null, I));
        I.addChild('H');
    }





    //--------------------------------------------------Depth------------------------------------------------------------
    @Test
    @DisplayName("Depth first pre-order traversal single node")
    void DepthFirstTraversalSingleNodeTest() {
        TreeNode<Integer> node = new TreeNode<>(1);
        assertThat(DepthFirstTraversal(node), is(Collections.singletonList(1)));

        var emptyList = DepthFirstTraversal(null);
        assertNotNull(emptyList);
        assertEquals(0, emptyList.size());
    }
    @Test
    @DisplayName("Depth first pre-order traversal tree")
    void DepthFirstTraversalTreeTest() {
        assertThat(DepthFirstTraversal(root), is(Arrays.asList('F', 'B', 'A', 'D', 'C', 'E', 'G', 'I', 'H')));
    }




    //---------------------------------------------------Breadth---------------------------------------------------------

    @Test
    @DisplayName("Breadth first traversal single node")
    void BreadthFirstTraversalSingleNodeTest() {
        TreeNode<Integer> node = new TreeNode<>(6);
        assertThat(BreadthFirstTraversal(node), is(Collections.singletonList(6)));

        var emptyList = BreadthFirstTraversal(null);
        assertNotNull(emptyList);
        assertEquals(0, emptyList.size());
    }

    @Test
    @DisplayName("Breadth first traversal tree")
    void BreadthFirstTraversalTreeTest() {
        assertThat(BreadthFirstTraversal(root), is(Arrays.asList('F', 'B', 'G', 'A', 'D', 'I', 'C', 'E', 'H')));
    }




}
