import Trees.TreeList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("List tree")
public class TreeList__TEST {
    @Test
    @DisplayName("Empty")
    void ListTreeEmptyTest() {
        TreeList<Integer, String> atd = new TreeList<>();
        assertNotNull(atd);
        assertEquals(0, atd.size());
        assertTrue(atd.isEmpty());
    }

    @Test
    @DisplayName("Put one element")
    void ListTreePutOneElementTest() {
        TreeList<Integer, String> listTree = new TreeList<>();
        assertFalse(listTree.contains(25));
        listTree.put(25, "TeSt");
        assertTrue(listTree.contains(25));
        assertEquals("TeSt", listTree.get(25));
        assertEquals(1, listTree.size());
        assertFalse(listTree.isEmpty());
    }


    @Test
    @DisplayName("Remove elements")
    void ListTreeRemoveElementsTest() {
        TreeList<Integer, String> listTree = new TreeList<>();
        listTree.put(1001, "Something");
        listTree.put(40, "test");
        listTree.put(0, "value");
        listTree.put(-5, "");
        assertEquals(4, listTree.size());
        assertEquals("", listTree.remove(-5));
        assertFalse(listTree.contains(-5));
        assertEquals("test", listTree.remove(40));
        assertFalse(listTree.contains(40));
        assertEquals(2, listTree.size());
        assertEquals("Something", listTree.remove(1001));
        assertEquals("value", listTree.remove(0));
        assertTrue(listTree.isEmpty());
    }
    @Test
    @DisplayName("Put the equal elements")
    void ListTreePutEqualElementsTest() {
        TreeList<Integer, String> listTree = new TreeList<>();
        listTree.put(25, "Test");
        listTree.put(25, "Another data");
        assertEquals(1, listTree.size());
        assertEquals("Test", listTree.get(25));
    }

    @Test
    @DisplayName("Remove nonexistent element")
    void ListTreeRemoveNonexistentElementTest() {
        TreeList<Integer, String> listTree = new TreeList<>();
        assertNull(listTree.remove(12));
    }
    @Test
    @DisplayName("Put 10 elements and Remove")
    void ListTreePut10ElementsAndRemoveTest() {
        TreeList<Integer, Double> listTree = new TreeList<>();
        IntStream.range(0, 10).forEach(x -> listTree.put(x, x * 10.3405));
        assertEquals(10, listTree.size());
        IntStream.range(0, 10).filter(x -> x % 2 == 0).forEach(listTree::remove);
        assertEquals(5, listTree.size());
    }

}