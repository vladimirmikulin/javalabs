package Trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


public class Traversals {

    public static <T> List<T> DepthFirstTraversal(TreeNode<T> root) {
        List<T> list = new ArrayList<>();
        DepthFirstTraversalHelper(root, list);
        return list;
    }

    private static <T> void DepthFirstTraversalHelper(TreeNode<T> node, List<T> list) {
        if (node != null) {
            list.add(node.getData());
            for (var subNode : node) {
                DepthFirstTraversalHelper(subNode, list);
            }
        }
    }

    public static <T> List<T> BreadthFirstTraversal(TreeNode<T> root) {
        List<T> list = new ArrayList<>();
        if (root != null) {
            Queue<TreeNode<T>> queue = new LinkedList<>();
            queue.add(root);
            while (!queue.isEmpty()) {
                var current = queue.remove();
                if (current != null) {
                    list.add(current.getData());
                    queue.addAll(current.getChildren());
                }
            }
        }
        return list;
    }
}
