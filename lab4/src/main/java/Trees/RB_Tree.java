package Trees;

import org.javatuples.Pair;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;


public class RB_Tree<K, V> extends TreeNode<Pair<K, V>> {
    public static final boolean RED = true;
    public static final boolean BLACK = false;
    private boolean isRed;

    public RB_Tree(Pair<K, V> data) {
        super(data);
        isRed = true;
        super.setChildren(new LinkedList<>(Arrays.asList(null, null)));
    }

    public boolean isRed() {
        return isRed;
    }

    public void setColor(boolean isRed) {
        this.isRed = isRed;
    }

    public RB_Tree<K, V> getLeftChild() {
        return (RB_Tree<K, V>) getChild(0);
    }

    public RB_Tree<K, V> getRightChild() {
        return (RB_Tree<K, V>) getChild(1);
    }

    public void setLeftChild(TreeNode<Pair<K, V>> child) {
        setChild(0, child);
    }

    public void setRightChild(TreeNode<Pair<K, V>> child) {
        setChild(1, child);
    }

    public RB_Tree<K, V> getGrandparent() {
        var parent = getParent();
        if (parent != null) {
            return (RB_Tree<K, V>) parent.getParent();
        } else {
            return null;
        }
    }

    public RB_Tree<K, V> getUncle() {
        var grandparent = getGrandparent();
        var parent = getParent();
        if (grandparent != null) {
            if (parent.equals(grandparent.getLeftChild())) {
                return grandparent.getRightChild();
            } else {
                return grandparent.getLeftChild();
            }
        } else {
            return null;
        }
    }

    public RB_Tree<K, V> getSibling() {
        var parent = (RB_Tree<K, V>) getParent();
        if (parent == null) {
            return null;
        } else if (this.equals(parent.getLeftChild())) {
            return parent.getRightChild();
        } else {
            return parent.getLeftChild();
        }
    }

    @Override
    public void setChildren(List<TreeNode<Pair<K, V>>> children) { }

    @Override
    public void addChildren(Collection<Pair<K, V>> children) { }

    @Override
    public void addChild(Pair<K, V> child) { }
}