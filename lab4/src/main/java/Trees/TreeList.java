package Trees;

import org.javatuples.Pair;
import java.util.*;

public class TreeList<K  extends Comparable<K>, V>{
private static final int TREEIFY_THRESHOLD = 8;
private long count = 0;
private RB_Tree<K, V> root = null;

public void put(K key, V value) {
        if (!contains(key) && key != null) {
        count++;
        if (count == TREEIFY_THRESHOLD) {
        treeify();
        }
        var dataPair = new Pair<>(key, value);
        if (shouldBeTree()) {
        insertCase1(insertNodeInBST(dataPair));
        } else {
        insertNodeInList(dataPair);
        }
        }
        }

public V get(K key) {
        var node = findNodeInListOrTree(key);
        return node == null ? null : node.getData().getValue1();
        }

public boolean isEmpty() {
        return count == 0;
        }

public V remove(K key) {
        var node = findNodeInListOrTree(key);
        if (node != null) {
        if (count == TREEIFY_THRESHOLD) {
        listify();
        }
        if (shouldBeTree()) {
        deleteNodeFromTree(node);
        } else {
        deleteNodeFromList(node);
        }
        count--;
        return node.getData().getValue1();
        }
        return null;
        }

public boolean contains(K key) {
        return findNodeInListOrTree(key) != null;
        }

public long size() {
        return count;
        }

private boolean shouldBeTree() {
        return count >= TREEIFY_THRESHOLD;
        }

private Pair<RB_Tree<K, V>, RB_Tree<K, V>> findNodeAndAncestorInListOrTree(
        K expectedKey, RB_Tree<K, V> curNode, RB_Tree<K, V> prevNode) {
        if (curNode != null && expectedKey != null) {
        var actualKey = curNode.getData().getValue0();
        if (expectedKey.equals(actualKey)) {
        return new Pair<>(curNode, prevNode);
        } else if (shouldBeTree() && expectedKey.compareTo(actualKey) < 0) {
        return findNodeAndAncestorInListOrTree(expectedKey, curNode.getLeftChild(), curNode);
        } else {
        return findNodeAndAncestorInListOrTree(expectedKey, curNode.getRightChild(), curNode);
        }
        }
        return new Pair<>(null, prevNode);
        }

private RB_Tree<K, V> findNodeInListOrTree(K expectedKey) {
        return findNodeAndAncestorInListOrTree(expectedKey, root, null).getValue0();
        }

private void insertNodeInList(Pair<K, V> data) {
        var node = new RB_Tree<>(data);
        node.setRightChild(root);
        if (root != null) {
        root.setParent(node);
        }
        root = node;
        }

private RB_Tree<K, V> insertNodeInBST(Pair<K, V> data) {
        var pair = findNodeAndAncestorInListOrTree(data.getValue0(), root, null);
        var parent = pair.getValue1();
        var newNode = new RB_Tree<>(data);
        if (parent == null) {
        root = newNode;
        } else {
        newNode.setParent(parent);
        if (data.getValue0().compareTo(parent.getData().getValue0()) < 0) {
        parent.setLeftChild(newNode);
        } else {
        parent.setRightChild(newNode);
        }
        }
        return newNode;
        }

private void deleteNodeFromList(RB_Tree<K, V> node) {
        var parent = (RB_Tree<K, V>) node.getParent();
        var child = node.getRightChild();
        node.setParent(null);
        node.setRightChild(null);
        if (parent == null) {
        root = child;
        if (root != null) {
        root.setParent(null);
        }
        } else {
        parent.setRightChild(child);
        if (child != null) {
        child.setParent(parent);
        }
        }
        }

private void deleteNodeFromTree(RB_Tree<K, V> node) {
        if (node.getLeftChild() != null && node.getRightChild() != null) {
        var minimumNode = getMinimumNodeHelper(node.getRightChild());
        node.setData(minimumNode.getData());
        deleteOneChild(minimumNode);
        } else {
        deleteOneChild(node);
        }
        }

private RB_Tree<K, V> getMinimumNodeHelper(RB_Tree<K, V> node) {
        if (node != null) {
        var leftNode = node.getLeftChild();
        if (leftNode == null) {
        return node;
        } else {
        return getMinimumNodeHelper(leftNode);
        }
        }
        return null;
        }

private void listify() {
        var temp = root;
        root = null;
        listifyHelper(temp);
        }

private void listifyHelper(RB_Tree<K, V> node) {
        if (node != null) {
        var pair = node.getData();
        insertNodeInList(new Pair<>(pair.getValue0(), pair.getValue1()));
        for (var subNode : node) {
        listifyHelper((RB_Tree<K, V>) subNode);
        }
        }
        }

private void treeify() {
        var temp = root;
        root = null;
        treeifyHelper(temp);
        }

private void treeifyHelper(RB_Tree<K, V> node) {
        if (node != null) {
        var pair = node.getData();
        insertCase1(insertNodeInBST(new Pair<>(pair.getValue0(), pair.getValue1())));
        treeifyHelper(node.getRightChild());
        }
        }

private void insertCase1(RB_Tree<K, V> node) {
        if (node.getParent() == null) {
        node.setColor(RB_Tree.BLACK);
        } else {
        insertCase2(node);
        }
        }

private void insertCase2(RB_Tree<K, V> node) {
        var parent = (RB_Tree<K, V>) node.getParent();
        if (parent.isRed()) {
        insertCase3(node);
        }
        }

private void insertCase3(RB_Tree<K, V> node) {
        var uncle = node.getUncle();
        if (uncle != null && uncle.isRed()) {
        var parent = (RB_Tree<K, V>) node.getParent();
        var grandparent = node.getGrandparent();
        parent.setColor(RB_Tree.BLACK);
        uncle.setColor(RB_Tree.BLACK);
        grandparent.setColor(RB_Tree.RED);
        insertCase1(grandparent);
        } else {
        insertCase4(node);
        }
        }

private void insertCase4(RB_Tree<K, V> node) {
        var grandparent = node.getGrandparent();
        var parent = (RB_Tree<K, V>) node.getParent();
        if (node.equals(parent.getRightChild()) && parent.equals(grandparent.getLeftChild())) {
        rotateLeft(parent);
        node = node.getLeftChild();
        } else if (node.equals(parent.getLeftChild()) &&  parent.equals(grandparent.getRightChild())) {
        rotateRight(parent);
        node = node.getRightChild();
        }
        insertCase5(node);
        }

private void insertCase5(RB_Tree<K, V> node) {
        var grandparent = node.getGrandparent();
        var parent = (RB_Tree<K, V>) node.getParent();
        parent.setColor(RB_Tree.BLACK);
        grandparent.setColor(RB_Tree.RED);
        if (node.equals(parent.getLeftChild()) && parent.equals(grandparent.getLeftChild())) {
        rotateRight(grandparent);
        } else {
        rotateLeft(grandparent);
        }
        }

private void replaceNode(RB_Tree<K, V> node, RB_Tree<K, V> child) {
        var parent = (RB_Tree<K, V>) node.getParent();
        if (child != null) {
        child.setParent(parent);
        }
        if (node.equals(parent.getLeftChild())) {
        parent.setLeftChild(child);
        } else {
        parent.setRightChild(child);
        }
        }

private void deleteOneChild(RB_Tree<K, V> node) {
        var child = (node.getRightChild() == null) ? node.getLeftChild() : node.getRightChild();
        replaceNode(node, child);
        if (!node.isRed() && child != null) {
        if (child.isRed()) {
        child.setColor(RB_Tree.BLACK);
        } else {
        deleteCase1(child);
        }
        }
        }

private void deleteCase1(RB_Tree<K, V> node) {
        if (node.getParent() != null) {
        deleteCase2(node);
        }
        }

private void deleteCase2(RB_Tree<K, V> node) {
        var sibling = node.getSibling();
        var parent = (RB_Tree<K, V>) node.getParent();
        if (sibling.isRed()) {
        parent.setColor(RB_Tree.RED);
        sibling.setColor(RB_Tree.BLACK);
        if (node.equals(parent.getLeftChild())) {
        rotateLeft(parent);
        } else {
        rotateRight(parent);
        }
        }
        deleteCase3(node);
        }

private void deleteCase3(RB_Tree<K, V> node) {
        var sibling = node.getSibling();
        var parent = (RB_Tree<K, V>) node.getParent();
        if (!parent.isRed() && !sibling.isRed()
        && !sibling.getLeftChild().isRed() && !sibling.getRightChild().isRed()) {
        sibling.setColor(RB_Tree.RED);
        deleteCase1(parent);
        } else {
        deleteCase4(node);
        }
        }

private void deleteCase4(RB_Tree<K, V> node) {
        var sibling = node.getSibling();
        var parent = (RB_Tree<K, V>) node.getParent();
        if (parent.isRed() && !sibling.isRed()
        && !sibling.getLeftChild().isRed() && !sibling.getRightChild().isRed()) {
        sibling.setColor(RB_Tree.BLACK);
        } else {
        deleteCase5(node);
        }
        }

private void deleteCase5(RB_Tree<K, V> node) {
        var sibling = node.getSibling();
        var parent = (RB_Tree<K, V>) node.getParent();
        if (!sibling.isRed()) {
        if (node.equals(parent.getLeftChild())
        && !sibling.getRightChild().isRed() && sibling.getLeftChild().isRed()) {
        sibling.setColor(RB_Tree.RED);
        sibling.getLeftChild().setColor(RB_Tree.BLACK);
        rotateRight(sibling);
        } else if (node.equals(parent.getRightChild())
        && !sibling.getLeftChild().isRed() && sibling.getRightChild().isRed()) {
        sibling.setColor(RB_Tree.RED);
        sibling.getRightChild().setColor(RB_Tree.BLACK);
        rotateLeft(sibling);
        }
        }
        deleteCase6(node);
        }

private void deleteCase6(RB_Tree<K, V> node) {
        var sibling = node.getSibling();
        var parent = (RB_Tree<K, V>) node.getParent();
        sibling.setColor(parent.isRed());
        parent.setColor(RB_Tree.BLACK);
        if (node.equals(parent.getLeftChild())) {
        sibling.getRightChild().setColor(RB_Tree.BLACK);
        rotateLeft(parent);
        } else {
        sibling.getLeftChild().setColor(RB_Tree.BLACK);
        rotateRight(parent);
        }
        }

private void rotateLeft(RB_Tree<K, V> node) {
        var pivot = node.getRightChild();
        var parent = (RB_Tree<K, V>) node.getParent();
        pivot.setParent(parent);
        if (root.equals(node)) {
        root = pivot;
        }
        changeParentChild(pivot, node, parent);
        node.setRightChild(pivot.getLeftChild());
        if (pivot.getLeftChild() != null) {
        pivot.getLeftChild().setParent(node);
        }
        node.setParent(pivot);
        pivot.setLeftChild(node);
        }

private void rotateRight(RB_Tree<K, V> node) {
        var pivot = node.getLeftChild();
        var parent = (RB_Tree<K, V>) node.getParent();
        pivot.setParent(parent);
        if (root.equals(node)) {
        root = pivot;
        }
        changeParentChild(pivot, node, parent);
        node.setLeftChild(pivot.getRightChild());
        if (pivot.getRightChild() != null) {
        pivot.getRightChild().setParent(node);
        }
        node.setParent(pivot);
        pivot.setRightChild(node);
        }

private static <K, V> void changeParentChild(
        RB_Tree<K, V> pivot, RB_Tree<K, V> node, RB_Tree<K, V> parent) {
        if (parent != null) {
        if (parent.getLeftChild().equals(node)) {
        parent.setLeftChild(pivot);
        } else {
        parent.setRightChild(pivot);
        }
        }
        }
}
