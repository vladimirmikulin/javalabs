package Trees;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class TreeNode<T> implements Iterable<TreeNode<T>> {
    private static final int INITIAL_CHILDREN_CAPACITY = 2;
    private T data;
    private TreeNode<T> parent;
    private List<TreeNode<T>> children;

    public TreeNode(T data) {
        this.data = data;
        this.children = new ArrayList<>(INITIAL_CHILDREN_CAPACITY);
    }

    public TreeNode(T data, TreeNode<T> parent) {
        this(data);
        if (parent != null) {
            this.parent = parent;
            parent.children.add(this);
        }
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public void setParent(TreeNode<T> parent) {
        this.parent = parent;
    }

    public List<TreeNode<T>> getChildren() {
        return new ArrayList<>(children);
    }

    public void setChildren(List<TreeNode<T>> children) {
        if (children != null) {
            this.children = new ArrayList<>(children);
        }
    }

    public void setChild(int index, TreeNode<T> child) {
        if (index < 0 || index >= children.size()) {
            throw new IndexOutOfBoundsException();
        }
        children.set(index, child);
    }

    public TreeNode<T> getChild(int index) {
        if (index < 0 || index >= children.size()) {
            throw new IndexOutOfBoundsException();
        }
        return children.get(index);
    }

    public void addChildren(Collection<T> children) {
        if (children != null) {
            children.forEach(x -> new TreeNode<>(x, this));
        }
    }

    public void addChild(T child) {
        new TreeNode<>(child, this);
    }

    @Override
    public Iterator<TreeNode<T>> iterator() {
        return children.iterator();
    }
}
