package FuncSets;

public interface FuncSet<T> {
    boolean contains(T element);
}
