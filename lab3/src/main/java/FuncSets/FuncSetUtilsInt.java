package FuncSets;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

public class FuncSetUtilsInt {

    public static boolean forAll(FuncSet<Integer> s, Predicate<Integer> p) {
        return forAllHelper(s, p, -1000, 1000);
    }

    private static boolean forAllHelper(FuncSet<Integer> s, Predicate<Integer> p, int i, int stop) {
        if (i > stop) {
            return true;
        }
        if (s.contains(i) && !p.test(i)) {
            return false;
        }
        return forAllHelper(s, p, ++i, stop);
    }

    public static boolean exists(FuncSet<Integer> s, Predicate<Integer> p) {
        return !forAll(s, x -> !p.test(x));
    }


    public static <R> FuncSet<R> map(FuncSet<Integer> s, Function<Integer, R> p) {
        return x -> exists(s, y -> Objects.equals(p.apply(y), x));
    }

    //------------------------------ exist one

    private static boolean forAllHelper___New(FuncSet<Integer> s, Predicate<Integer> p, int i, int stop) {
        if (i > stop) {
            return false;
        }
        if (s.contains(i) && p.test(i)) {
            return true;
        }
        return forAllHelper(s, p, ++i, stop);
    }


    public static boolean exists___New(FuncSet<Integer> s, Predicate<Integer> p) {
        return forAll(s, x -> p.test(x));
    }

}
