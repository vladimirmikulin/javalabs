package FuncSets;

import java.util.Objects;
import java.util.function.Predicate;

public class FuncSetUtils {

    public static <T> FuncSet<T> empty() {
        return x -> false;
    }

    public static <T> FuncSet<T> singletonSet(T val) {
        return x -> Objects.equals(val, x);
    }

    public static <T> FuncSet<T> union(FuncSet<T> s, FuncSet<T> t) {
        return x -> s.contains(x) || t.contains(x);
    }

    public static <T> FuncSet<T> intersect(FuncSet<T> s, FuncSet<T> t) {
        return x -> s.contains(x) && t.contains(x);
    }

    public static <T> FuncSet<T> diff(FuncSet<T> s, FuncSet<T> t) {
        return x -> s.contains(x) && !t.contains(x);
    }

    public static <T> FuncSet<T> filter(FuncSet<T> s, Predicate<T> p) {
        return x -> s.contains(x) && p.test(x);
    }
}

