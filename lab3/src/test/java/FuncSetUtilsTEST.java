import FuncSets.FuncSet;
import FuncSets.FuncSetUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static FuncSets.FuncSetUtils.*;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Functional Set Utils")
    public class FuncSetUtilsTEST {
        @Test
        @DisplayName("Empty set test")
        void emptySetTest() {
            FuncSet<Integer> set = FuncSetUtils.empty();
            assertNotNull(set);
            FuncSet<Character> characterSet = empty();
            assertNotNull(characterSet);
            assertFalse(characterSet.contains('j'));
            assertFalse(characterSet.contains('%'));
        }

        @Test
        @DisplayName("Singleton set test")
        void singletonSetTest() {
            FuncSet<Integer> set = singletonSet(6);
            assertNotNull(set);
            assertTrue(set.contains(6));
            var stringSet = singletonSet("");
            assertTrue(stringSet.contains(""));
            var characterSet = singletonSet('C');
            assertTrue(characterSet.contains('C'));
            assertFalse(characterSet.contains('c'));
        }

        @Test
        @DisplayName("Union test")
        void unionTest() {
            int[] numbers = new int [] { -657, 34 };
            var numberSetOne = singletonSet(numbers[0]);
            var numberSetAnother = singletonSet(numbers[1]);
            var unionNumber = FuncSetUtils.union(numberSetOne, numberSetAnother);
            assertNotNull(unionNumber);
            assertTrue(unionNumber.contains(numbers[0]));
            assertTrue(unionNumber.contains(numbers[1]));
        }

        @Test
        @DisplayName("Intersect test")
        void intersectTest() {
            String test = "Test!";
            var stringSet = singletonSet(test);
            var stringSetSame = singletonSet(test);
            assertNotNull(stringSet);
            assertTrue(FuncSetUtils.intersect(stringSet, stringSetSame).contains(test));
            char[] chars = new char[] { 'Q', 'f', '0'};
            var charSetOne = FuncSetUtils.union(singletonSet(chars[0]), singletonSet(chars[1]));
            var charSetAnother = FuncSetUtils.union(singletonSet(chars[1]), singletonSet(chars[2]));
            var intersectChars = FuncSetUtils.intersect(charSetOne, charSetAnother);
            assertTrue(intersectChars.contains(chars[1]));
            assertFalse(intersectChars.contains(chars[0]));
            assertFalse(intersectChars.contains(chars[2]));
        }
        @Test
        @DisplayName("Filter test")
        void filterTest() {
            var singleSet = singletonSet(Math.PI);
            assertNotNull(singleSet);
            assertFalse(filter(singleSet, x -> x != Math.PI).contains(Math.PI));
            assertTrue(filter(singleSet, x -> true).contains(Math.PI));
        }

        @Test
        @DisplayName("Diff test")
        void diffTest() {
            char[] chars = new char[] { 'u', 'R', '\0', 'q'};
            var charSetOne = FuncSetUtils.union(singletonSet(chars[0]), singletonSet(chars[1]));
            var charSetAnother = FuncSetUtils.union(singletonSet(chars[2]), singletonSet(chars[3]));
            charSetOne = FuncSetUtils.union(charSetOne, charSetAnother);
            var diffChars = FuncSetUtils.diff(charSetOne, charSetAnother);
            assertNotNull(diffChars);
            assertTrue(diffChars.contains(chars[1]));
            assertTrue(diffChars.contains(chars[0]));
            assertFalse(diffChars.contains(chars[2]));
            assertFalse(diffChars.contains(chars[3]));
        }


    }
