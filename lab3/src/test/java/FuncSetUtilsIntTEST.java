import FuncSets.FuncSet;
import FuncSets.FuncSetUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static FuncSets.FuncSetUtils.*;
import static FuncSets.FuncSetUtilsInt.*;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Functional Integer Set Utils")
public class FuncSetUtilsIntTEST {
    @Test
    @DisplayName("ForAll test")
    void forAllTest() {
        int[] numbers = new int [] { -896, 58 };
        var numberSetOne = singletonSet(numbers[0]);
        var numberSetAnother = singletonSet(numbers[1]);
        var unionNumber = FuncSetUtils.union(numberSetOne, numberSetAnother);
        assertTrue(forAll(unionNumber, x -> x >= -896 && x < 59));
        assertFalse(forAll(unionNumber, x -> x == 0));
    }

    @Test
    @DisplayName("Exists test")
    void existsTest() {
        int[] numbers = new int [] { -896, 58 };
        var numberSetOne = singletonSet(numbers[0]);
        var numberSetAnother = singletonSet(numbers[1]);
        var unionNumber = FuncSetUtils.union(numberSetOne, numberSetAnother);
        assertTrue(exists(unionNumber, x -> x == -896));
        assertFalse(exists(unionNumber, x -> x == 26));
    }

    @Test
    @DisplayName("Map test")
    void mapTest() {
        var singleSet = singletonSet(6);
        var mappedSingleSet = map(singleSet, x -> x * 13.45);
        assertNotNull(mappedSingleSet);
        assertTrue(mappedSingleSet.contains(6 * 13.45));
        int[] numbers = new int [] { -657, 34 };
        var numberSetOne = singletonSet(numbers[0]);
        var numberSetAnother = singletonSet(numbers[1]);
        var unionNumber = FuncSetUtils.union(numberSetOne, numberSetAnother);
        var mappedSingleSet2 = map(unionNumber, x -> x * 13.45);
        assertTrue(mappedSingleSet2.contains(34 * 13.45));
        assertTrue(mappedSingleSet2.contains(-657 * 13.45));
    }
}
